import os
from StringIO import StringIO
import boto3
import paramiko


def lambda_handler(event, context):

    PRIVATE_KEY = """"""
    ssh_username = 'mi9'
    ssh_host = 'mi9-mkt-stage1.campaign.adobe.com'
    ssh_dir = '/incoming'
    ssh_port = 22

    pkey = None

    if PRIVATE_KEY:
        pkey = paramiko.RSAKey.from_private_key(StringIO(PRIVATE_KEY))

    sftp, transport = connect_to_SFTP(
        hostname=ssh_host,
        port=ssh_port,
        username=ssh_username,
        pkey=pkey
    )
    s3 = boto3.client('s3')

    if ssh_dir:
        sftp.chdir(ssh_dir)

    with transport:
        for record in event['Records']:
            uploaded = record['s3']
            filename = uploaded['object']['key'].split('/')[-1]

            try:
                transfer_file(
                    s3_client=s3,
                    bucket=uploaded['bucket']['name'],
                    key=uploaded['object']['key'],
                    sftp_client=sftp,
                    sftp_dest=filename
                )
            except Exception:
                print 'Could not upload file to SFTP'
                raise

            else:
                print 'S3 file "{}" uploaded to SFTP successfully'.format(
                    uploaded['object']['key']
                )


def connect_to_SFTP(hostname, port, username, pkey):
    transport = paramiko.Transport((hostname, port))
    transport.connect(
        username=username,
        pkey=pkey
    )
    sftp = paramiko.SFTPClient.from_transport(transport)

    return sftp, transport


def transfer_file(s3_client, bucket, key, sftp_client, sftp_dest):
    """
    Download file from S3 and upload to SFTP
    """
    with sftp_client.file(sftp_dest, 'w') as sftp_file:
        s3_client.download_fileobj(
            Bucket=bucket,
            Key=key,
            Fileobj=sftp_file
        )
